pge_dir=$(cd "$(dirname "$0")" ; pwd -P)
app_dir=$(dirname ${pge_dir})
ver=1.1.0

# Create conda env
conda create -n benthos -y -c conda-forge python=3.8 gdal=3.6 awscli=1 make=4 gcc=12
source activate benthos

# Copy the benthos repo into the app directory and rename to "benthos"
pushd $app_dir
aws s3 cp s3://sister-ops-registry/packages/benthic_inversion/benthos-${ver}.tar.gz benthos-${ver}.tar.gz
tar xzvf benthos-${ver}.tar.gz
rm -f benthos-${ver}.tar.gz
mv benthos-${ver} benthos

# Install benthos and dependencies
pushd benthos
pip install -r requirements.txt
pip install hy_tools_lite==1.1.1
pip install Pillow==9.2.0
make clean
make
