#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SISTER
Space-based Imaging Spectroscopy and Thermal PathfindER
Author: Winston Olson-Duvall
"""

import json
import os
import shutil
import subprocess
import sys

import numpy as np

from PIL import Image

import hytools_lite as ht


def get_benthrfl_basename(corfl_basename, crid):
    # Replace product type
    tmp_basename = corfl_basename.replace("L2A_CORFL", "L2B_BENTHRFL")
    # Split, remove old CRID, and add new one
    tokens = tmp_basename.split("_")[:-1] + [str(crid)]
    return "_".join(tokens)


def generate_metadata(run_config, json_path, new_metadata):

    metadata = run_config['metadata']
    for key, value in new_metadata.items():
        metadata[key] = value
    with open(json_path, 'w') as out_obj:
        json.dump(metadata, out_obj, indent=4)


def generate_quicklook(input_file, output_path):

    img = ht.HyTools()
    img.read_file(input_file)

    band3 = img.get_wave(440)
    band2 = img.get_wave(550)
    band1 = img.get_wave(660)
    rgb = np.stack([band1, band2, band3])
    rgb[rgb == img.no_data] = np.nan

    rgb = np.moveaxis(rgb, 0, -1).astype(float)
    bottom = np.nanpercentile(rgb, 5, axis=(0, 1))
    top = np.nanpercentile(rgb, 95, axis=(0, 1))
    rgb = np.clip(rgb,bottom,top)
    rgb = (rgb - np.nanmin(rgb, axis=(0, 1)))/(np.nanmax(rgb, axis= (0, 1)) - np.nanmin(rgb, axis= (0, 1)))
    rgb = (rgb*255).astype(np.uint8)

    im = Image.fromarray(rgb)
    im.save(output_path)


def main():
    """
        This function takes as input the path to an inputs.json file and exports a run config json
        containing the arguments needed to run the SISTER ISOFIT PGE.

    """
    in_file = sys.argv[1]

    # Read in runconfig
    print("Reading in runconfig")
    with open(in_file, "r") as f:
        run_config = json.load(f)

    # Make work dir
    print("Making work directory")
    if not os.path.exists("work"):
        subprocess.run("mkdir work", shell=True)

    # Make output dir
    print("Making output directory")
    if not os.path.exists("output"):
        subprocess.run("mkdir output", shell=True)

    # Define paths and variables
    sister_benth_inv_dir = os.path.abspath(os.path.dirname(__file__))
    benthos_dir = os.path.join(os.path.dirname(sister_benth_inv_dir), "benthos")

    corfl_basename = None
    frcov_basename = None
    for file in run_config["inputs"]["file"]:
        if "corrected_reflectance_dataset" in file:
            corfl_basename = os.path.basename(file["corrected_reflectance_dataset"])
        if "fractional_cover_dataset" in file:
            frcov_basename = os.path.basename(file["fractional_cover_dataset"])
    benthrfl_basename = get_benthrfl_basename(corfl_basename, run_config["inputs"]["config"]["crid"])
    depth_basename = f"{benthrfl_basename}_DEPTH"

    corfl_envi_path = f"input/{corfl_basename}/{corfl_basename}.bin"
    frcov_tiff_path = f"input/{frcov_basename}/{frcov_basename}.tif"

    tmp_benthrfl_envi_path = f"work/{corfl_basename}_Rb"
    tmp_depth_envi_path = f"work/{corfl_basename}_depth"

    log_path = f"output/{benthrfl_basename}.log"

    # Run benthic inversion
    benthos_exe = f"{benthos_dir}/benthos_parallel.py"
    nproc = run_config["inputs"]["config"]["nproc"]
    cmd = [
        "python",
        benthos_exe,
        corfl_envi_path,
        frcov_tiff_path,
        "work",
        f"--nproc {nproc}",
        "--verbose",
        ">>",
        log_path
    ]
    print("Running benthos command: " + " ".join(cmd))
    subprocess.run(" ".join(cmd), shell=True)

    # Copy benthic reflectance and depth to output directory
    shutil.copyfile(tmp_benthrfl_envi_path, f"output/{benthrfl_basename}.bin")
    shutil.copyfile(f"{tmp_benthrfl_envi_path}.hdr", f"output/{benthrfl_basename}.hdr")
    shutil.copyfile(tmp_depth_envi_path, f"output/{depth_basename}.bin")
    shutil.copyfile(f"{tmp_depth_envi_path}.hdr", f"output/{depth_basename}.hdr")

    # Generate metadata
    print("Generating metadata in .met.json files")
    generate_metadata(run_config,
                      f"output/{benthrfl_basename}.met.json",
                      {'product': 'BENTHRFL',
                       'processing_level': 'L2B',
                       'description': "Benthic reflectance"})

    generate_metadata(run_config,
                      f"output/{depth_basename}.met.json",
                      {'product': 'BENTHRFL_DEPTH',
                       'processing_level': 'L2B',
                       'description': 'Depth (m)'})

    # Generate quicklook
    print("Generating quicklook PNG")
    generate_quicklook(tmp_benthrfl_envi_path, f"output/{benthrfl_basename}.png")

    # Copy any remaining files to output
    print("Copying runconfig to output folder")
    shutil.copyfile("runconfig.json", f"output/{benthrfl_basename}.runconfig.json")


if __name__ == "__main__":
    main()
