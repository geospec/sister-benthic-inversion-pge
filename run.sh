#!/bin/bash

# Use benthos conda env from docker image
source activate benthos

# Get repository directory
REPO_DIR=$(cd "$(dirname "$0")"; pwd -P)

# Generate runconfig
python ${REPO_DIR}/generate_runconfig.py inputs.json

# Execute benthic-inversion
python ${REPO_DIR}/sister_benthic_inversion.py runconfig.json
